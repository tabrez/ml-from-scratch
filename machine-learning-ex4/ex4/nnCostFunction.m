function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices.
%
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);

% You need to return the following variables correctly
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% -------------------------------------------------------------

% =========================================================================



## J = calc_reg_cost(X, y, Theta1, Theta2, lambda);

## [Theta1_grad, Theta2_grad] = calc_grads(X, Theta1, Theta2, y, lambda);
K = num_labels;
yk = eye(K)(y,:);  ## yk is mxnum_labels: 5000x10

a_1 = [ones(m, 1) X];
z_2 = a_1 * Theta1';
a_2 = [ones(m, 1) sigmoid(z_2)];
z_3 = a_2 * Theta2';
hx = a_3 = sigmoid(z_3); ## hx
res = (yk .* log(hx)) + ((1 - yk) .* log(1 - hx));
J = (-1/m) * sum(sum(res));

reg_term = sum(sum(Theta1(:,2:end) .^ 2)) + sum(sum(Theta2(:,2:end) .^ 2));
J = J + (lambda / (2 * m)) * reg_term;

d_3 = a_3 - yk;
d_2 = (d_3 * Theta2) .* [ones(size(z_2, 1), 1), sigmoidGradient(z_2)];
d_2 = d_2(:,2:end);
Delta1 = d_2' * a_1;
Delta2 = d_3' * a_2;
Theta1_grad = (1/m) * Delta1 + (lambda/m) * [zeros(size(Theta1,1), 1), Theta1(:, 2:end)];
Theta2_grad = (1/m) * Delta2 + (lambda/m) * [zeros(size(Theta2,1), 1), Theta2(:, 2:end)];
% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];
endfunction

function [a_1, a_2, a_3, z_2, z_3] = ff_one_observation(X, theta1, theta2)
  ## theta1 is 25x401, theta2 is 10x26
  a_1 = [1, X]; ## a_1 is 1x401
  z_2 = a_1 * theta1'; ## z_2 is 1x25
  a_2 = sigmoid(z_2);
  a_2 = [1, a_2]; ## a_2 is 1x26
  z_3 = a_2 * theta2'; ## z_3 is 1x10
  a_3 = sigmoid(z_3); ## hx
endfunction

function [Theta1_grad, Theta2_grad] = calc_grads(X, Theta1, Theta2, y, lambda)
  ## Theta1 is 25x401, Theta2 is 10x26
  ## yk = map_y(y);
  K = length(unique(y));
  yk = eye(K)(y,:);  ## yk is 5000x10
  m = size(X, 1);
  init_Delta = false;

  for i=1:m
    [a_1, a_2, a_3, z_2, z_3] = ff_one_observation(X(i,:), Theta1, Theta2);
    ## a_1 is 1x401, a_2 & z_2 are 1x25, a_3 & z_3 are 1x10
    d_3 = a_3 - yk(i);
    ## d_3 is 1x10
    d_2 = (d_3 * Theta2) .* [1, sigmoidGradient(z_2)];
    ## d_2 is 1x26
    d_2 = d_2(2:end);
    ## d_2 is 1x25
    if init_Delta == false
      Delta1 = zeros(length(d_2'), length(a_1));
      Delta2 = zeros(length(d_3'), length(a_2));
      init_Delta = true;
    endif
    Delta1 = Delta1 + d_2' * a_1;
    Delta2 = Delta2 + d_3' * a_2;
  endfor
  Theta1_grad = (1/m) * Delta1 + (lambda/m) * [zeros(size(Theta1, 1), 1), Theta1(:, 2:end)];
  Theta2_grad = (1/m) * Delta2 + (lambda/m) * [zeros(size(Theta2, 1), 1), Theta2(:, 2:end)];
endfunction
