function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
  m = length(y); % number of training examples
  J_history = zeros(num_iters, 1);

  for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta.
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %

    ## theta = theta - [(alpha/m) * Sum((theta.X - y) * X)]
    ## X is mx2, theta is 2x1
    h = X * theta;
    ## h is mx1, y is mx1
    diff = h - y;
    ## diff is mx1, X is mx2
    prod = X' * diff;
    ## prod is 2x1
    term = (alpha/m) * prod;
    ## theta is 2x1, term is 2x1
    theta = theta - term;
    % ============================================================

    % Save the cost J in every iteration
    J_history(iter) = computeCost(X, y, theta);

  end

end
